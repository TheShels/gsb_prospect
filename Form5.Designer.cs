﻿namespace GSB_CR
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_Pra_Nom = new System.Windows.Forms.TextBox();
            this.tb_Pra_Prenom = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_Pra_Id = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_Pra_Adresse = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_Pra_Ville_CP = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_Pra_Notoriete = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_Pra_Visite = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_Pra_Code = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_CR_Fermer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tb_Pra_Nom
            // 
            this.tb_Pra_Nom.Location = new System.Drawing.Point(67, 12);
            this.tb_Pra_Nom.Name = "tb_Pra_Nom";
            this.tb_Pra_Nom.Size = new System.Drawing.Size(121, 20);
            this.tb_Pra_Nom.TabIndex = 16;
            // 
            // tb_Pra_Prenom
            // 
            this.tb_Pra_Prenom.Location = new System.Drawing.Point(67, 38);
            this.tb_Pra_Prenom.Name = "tb_Pra_Prenom";
            this.tb_Pra_Prenom.Size = new System.Drawing.Size(121, 20);
            this.tb_Pra_Prenom.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Nom : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Prenom : ";
            // 
            // tb_Pra_Id
            // 
            this.tb_Pra_Id.Location = new System.Drawing.Point(310, 12);
            this.tb_Pra_Id.Name = "tb_Pra_Id";
            this.tb_Pra_Id.Size = new System.Drawing.Size(121, 20);
            this.tb_Pra_Id.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(205, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Numéro id  : ";
            // 
            // tb_Pra_Adresse
            // 
            this.tb_Pra_Adresse.Location = new System.Drawing.Point(67, 64);
            this.tb_Pra_Adresse.Name = "tb_Pra_Adresse";
            this.tb_Pra_Adresse.Size = new System.Drawing.Size(121, 20);
            this.tb_Pra_Adresse.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Adresse :";
            // 
            // tb_Pra_Ville_CP
            // 
            this.tb_Pra_Ville_CP.Location = new System.Drawing.Point(67, 90);
            this.tb_Pra_Ville_CP.Name = "tb_Pra_Ville_CP";
            this.tb_Pra_Ville_CP.Size = new System.Drawing.Size(121, 20);
            this.tb_Pra_Ville_CP.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Ville : ";
            // 
            // tb_Pra_Notoriete
            // 
            this.tb_Pra_Notoriete.Location = new System.Drawing.Point(310, 38);
            this.tb_Pra_Notoriete.Name = "tb_Pra_Notoriete";
            this.tb_Pra_Notoriete.Size = new System.Drawing.Size(121, 20);
            this.tb_Pra_Notoriete.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(205, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Coef de Notoriete : ";
            // 
            // tb_Pra_Visite
            // 
            this.tb_Pra_Visite.Location = new System.Drawing.Point(310, 64);
            this.tb_Pra_Visite.Name = "tb_Pra_Visite";
            this.tb_Pra_Visite.Size = new System.Drawing.Size(121, 20);
            this.tb_Pra_Visite.TabIndex = 28;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(205, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Date de Visite : ";
            // 
            // tb_Pra_Code
            // 
            this.tb_Pra_Code.Location = new System.Drawing.Point(310, 90);
            this.tb_Pra_Code.Name = "tb_Pra_Code";
            this.tb_Pra_Code.Size = new System.Drawing.Size(121, 20);
            this.tb_Pra_Code.TabIndex = 30;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(205, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Type de Code : ";
            // 
            // btn_CR_Fermer
            // 
            this.btn_CR_Fermer.Location = new System.Drawing.Point(356, 116);
            this.btn_CR_Fermer.Name = "btn_CR_Fermer";
            this.btn_CR_Fermer.Size = new System.Drawing.Size(75, 23);
            this.btn_CR_Fermer.TabIndex = 32;
            this.btn_CR_Fermer.Text = "Fermer";
            this.btn_CR_Fermer.UseVisualStyleBackColor = true;
            this.btn_CR_Fermer.Click += new System.EventHandler(this.btn_CR_Fermer_Click);
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(442, 150);
            this.Controls.Add(this.btn_CR_Fermer);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tb_Pra_Code);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tb_Pra_Visite);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tb_Pra_Notoriete);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb_Pra_Ville_CP);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_Pra_Adresse);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_Pra_Id);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_Pra_Prenom);
            this.Controls.Add(this.tb_Pra_Nom);
            this.Name = "Form5";
            this.Text = "Détails du Praticien";
            this.Load += new System.EventHandler(this.Form5_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_Pra_Nom;
        private System.Windows.Forms.TextBox tb_Pra_Prenom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_Pra_Id;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_Pra_Adresse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_Pra_Ville_CP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_Pra_Notoriete;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_Pra_Visite;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_Pra_Code;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_CR_Fermer;
    }
}